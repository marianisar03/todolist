from django.shortcuts import render, redirect

from .models import Todo


def index(request):
    todos = Todo.objects.all()[:10]

    context = {
        'todos': todos
    }
    return render(request, 'index.html', context)


def delete(request, id):
    Todo.objects.get(id=id).delete()

    return redirect('/todos')


def details(request, id):
    todo = Todo.objects.get(id=id)

    context = {
        'todo': todo
    }
    return render(request, 'details.html', context)


def update(request, id):
    if request.method == 'GET':
        todo = Todo.objects.get(id=id)
        context = {
            'todo': todo
        }
        return render(request, 'update.html', context)
    else:
        todo = Todo.objects.get(id=id)
        todo.title = request.POST['title']
        todo.text = request.POST['text']

        todo.save()

        return redirect('/todos')


def add(request):
    if (request.method == 'POST'):
        title = request.POST['title']
        text = request.POST['text']

        todo = Todo(title=title, text=text)
        todo.save()

        return redirect('/todos')
    else:
        return render(request, 'add.html')
